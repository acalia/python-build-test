FROM debian:jessie

RUN apt update
RUN apt install -y python python-dev python-pip
RUN apt install -y python3 python3-dev python3-pip

RUN mkdir /opt/resources/
WORKDIR /opt/resources/

RUN git clone https://gitlab.cern.ch/coupling/lhc-coupling-analysis-service.git
RUN git clone --recursive https://gitlab.cern.ch/acalia/Beta-Beat.src.git
ENV BETA_BEAT_PROJECT_PATH /opt/resources/Beta-Beat.src/

WORKDIR /opt/resources/lhc-coupling-analysis-service

RUN python2 -m pip install numpy
RUN python3 -m pip install grpcio grpcio-tools urllib3

CMD ["python3", "lhc_coupling_service.py"]

EXPOSE 60051